import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  shareObj = {
    href: "https://kernherzele.firebaseapp.com/",
    image:"https://firebasestorage.googleapis.com/v0/b/kernherzele.appspot.com/o/images%2FAffiche-HKZ-A2.svg?alt=media&token=5af1b056-c79f-43ae-87ea-2767f5148964",
    title:"Kerncompetitie Herzele",
    description : "Onze kerncompetitie is op zoek naar jou!! Heb jij een paar vrienden die kunnen voetballen en wil je traditie ploegen als De Promillekes en De Sinner proberen van hun troon te stoten? Neem gerust contact op voor meer info. "
};

  constructor() { }

  ngOnInit() {
  }


}
