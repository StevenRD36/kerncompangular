import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {TeamService} from 'src/app/services/team.service';
import {Team} from 'src/app/domain/Team';
import {TeamRespService} from 'src/app/services/team-resp.service';
import {BaseUser} from 'src/app/domain/BaseUser';
import {StringHelper} from 'src/app/domain/string.helper';
import {Player} from 'src/app/domain/Player';
import {PlayerService} from 'src/app/services/player.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageService} from 'src/app/services/message.service';
import {AuthService} from 'src/app/services/auth.service';
import {Message} from 'src/app/domain/Message';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {
  successTeamMessage: string[];
  errorTeamMessage: string;
  errorPlayerMessage: string[];
  successPlayerMessage: string[];
  adminId: string;
  teamId: string;
  team: Team;
  errorMessage: string;
  teamResp: BaseUser;
  adminUser: BaseUser;
  displayedColumns: string[] = ['#', 'Voornaam', 'Naam', 'Geboortedatum', 'Aanvraag', 'Bevestigd', 'Admin', 'Actie'];
  players: Player[];
  toConfirm: number;
  confirm = new FormGroup({
    sendConfirm: new FormControl('', [Validators.required, Validators.minLength(3)])
  });

  isPlayerLoading: boolean;

  isTeamLoading: boolean;

  constructor(private activeRoute: ActivatedRoute, private teamService: TeamService,
              private teamRespService: TeamRespService, private playerService: PlayerService,
              private messageService: MessageService, private auth: AuthService) {
  }

  ngOnInit() {
    this.successTeamMessage = [];
    this.successPlayerMessage = [];
    this.errorPlayerMessage = [];
    this.isTeamLoading = false;
    this.isPlayerLoading = false;
    this.toConfirm = 0;
    this.adminId = this.activeRoute.snapshot.paramMap.get('id');
    this.teamId = this.activeRoute.snapshot.paramMap.get('teamId');
    this.findTeam(true);
    this.findPlayers();
    this.findAdminUser();
  }

  findTeam(fromInit: boolean) {
    this.teamService.findTeamById(this.teamId).then(
      snapshot => {
        this.team = snapshot;
        this.team.id = this.teamId;
        if (fromInit) {
          this.findTeamResp(this.team.resp);
        }
        this.toggleTeamLoading(false);
      });
  }

  getRespFullName() {
    return StringHelper.getFullName(this.teamResp.firstName, this.teamResp.lastName);
  }

  findTeamResp(id: string) {
    this.teamRespService.findTeamRespById(id).subscribe(
      data => {
        console.log(data);
        return this.teamResp = data as BaseUser;
      },
      err => {
        console.log('TeamResp not found');
      }
    );
  }

  findAdminUser() {
    this.teamRespService.findTeamRespById(this.adminId).subscribe(
      data => {
        console.log(data);
        return this.adminUser = data as BaseUser;
      },
      err => {
        console.log('Admin not found');
      }
    );
  }

  goBack() {
    this.toConfirm = 0;
    this.confirm.controls.sendConfirm = new FormControl('', [Validators.required, Validators.minLength(3)]);
  }

  findPlayers() {
    this.players = [];
    this.playerService.findAllPlayersForTeam(this.teamId).subscribe(
      actionArray => {
        this.players = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as Player;
        });
      },
      reject => {
        this.errorMessage = 'Oeps er is iets mis gelopen met de spelers op te halen';
      }
    );
  }

  openForm(i: number) {
    console.log(i);
    this.toConfirm = i;
  }

  confirmTeam() {
    this.toggleTeamLoading(true);
    switch (this.toConfirm) {
      case 1 :
        this.updateTeamForSuccess();
        break;
      case 2 :
        this.updateTeamForChange();
        break;
      case 3 :
        this.deleteTeam();
        break;
    }
  }

  sendMessage(subjectForMessage: string, bodyForMessage: string) {
    const message = {
      receiver: this.teamResp.id,
      receiverName: StringHelper.getFullName(this.teamResp.firstName, this.teamResp.lastName),
      sender: this.adminUser.id,
      senderName: StringHelper.getFullName(this.adminUser.firstName, this.adminUser.lastName),
      subject: subjectForMessage,
      body: bodyForMessage,
      seen: false,
      date: new Date(Date.now()),
      dateSeen: null
    } as Message;

    console.log(message);

    this.messageService.addMessage(message).then(
      res => {
        this.successTeamMessage.push('Bericht werd verstuurd naar de teamverantwoordelijke');
      },
      err => {
        console.log(err);
        this.errorTeamMessage = 'Er is iets misgegaan met het bericht te verzenden, probeer het later opnieuw';
      }
    );
  }

  updateTeamForSuccess() {
    this.team.changeRequest = false;
    this.team.confirmedDate = new Date(Date.now());
    this.team.confirmAdmin = StringHelper.getFullName(this.adminUser.firstName, this.adminUser.lastName);
    this.team.confirmed = true;
    console.log(this.team);
    const messBody: string = this.confirm.controls.sendConfirm.value;
    this.updateTeam('Team ' + this.team.name + ' werd goedgekeurd', messBody);
  }

  updateTeam(message: string, messageBody: string) {
    console.log(this.team);
    this.teamService.updateTeam(this.team).then(
      res => {
        this.sendMessage('Inschrijving Bevestigd', messageBody);
        this.successTeamMessage.push(message);
        this.toConfirm = 0;
        this.findTeam(true);
      },
      err => {
        console.log(err);
        this.errorTeamMessage = 'Er ging iets fout bij het updaten van het team';
      });
  }

  updateTeamForChange() {
    this.team.changeRequest = true;
    this.team.changeRequestDate = new Date(Date.now());
    this.team.changeRequestAdmin = StringHelper.getFullName(this.adminUser.firstName, this.adminUser.lastName);
    const messBody: string = this.confirm.controls.sendConfirm.value;
    this.updateTeam('Aanvraag tot wijziging werd verstuurd', messBody);
  }

  deleteTeam() {
    //TODO
  }

  toggleTeamLoading(load: boolean) {
    this.isTeamLoading = load;
  }

  togglePlayerLoad(load: boolean) {
    this.isPlayerLoading = load;
  }

  declinePlayer(player: Player) {
    //  TODO
    //  open en alert to decline the player
  }

  


  confirmPlayer(player: Player) {
    this.togglePlayerLoad(true);
    player.confirmDate = new Date(Date.now());
    player.confirmed = true;
    player.confirmedAdminName = StringHelper.getFullName(this.adminUser.firstName, this.adminUser.lastName);
    console.log(player);
    this.playerService.updatePlayer(player).then(
      res => {
        console.log('player updated successfull');
        const body = 'Speler : ' + player.firstName + ' ' + player.lastName + ' werd goedgekeurd';
        this.successPlayerMessage.push(body);
        this.togglePlayerLoad(false);
        this.sendMessage('Speler goedgekeurd', body);
      },
      err => {
        console.log(err);
        this.errorPlayerMessage.push('Er is iets misgegaan bij het goedkeuren van speler ' + player.firstName + ' ' + player.lastName);
        this.togglePlayerLoad(false);
      });
  }
}

