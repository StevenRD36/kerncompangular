import {Component, OnInit, Input} from '@angular/core';
import {Team} from '../../domain/Team';


@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  @Input() teams: Team[];
  selectedTeam: Team;
  title: string;
  displayedColumns = ['#', 'Team', 'Verantwoordelijke', 'Registratie', 'Goedkeuring', 'Acties'];

  constructor() {
  }

  ngOnInit() {
    if (this.selectedTeam != null) {
      this.title = this.selectedTeam.name;
    } else {
      this.title = 'Teams';
    }
  }


  selectTeam(team: Team) {
    this.selectedTeam = team;
  }
}
