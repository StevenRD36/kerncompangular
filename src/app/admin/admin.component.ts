import { Component, OnInit } from '@angular/core';
import { BaseUser } from '../domain/BaseUser';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { TeamRespService } from '../services/team-resp.service';
import { Timestamp } from 'rxjs';
import { Team } from '../domain/Team';
import { TeamService } from '../services/team.service';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  id?;
  errorMessage:string;
  adminUser : BaseUser;
  teams:Team[];

  constructor(
    private activeRoute:ActivatedRoute,
    private teamService:TeamService,
    private teamRespService:TeamRespService) { }

  ngOnInit() {
    this.id = this.activeRoute.snapshot.paramMap.get("id");
    console.log(this.id);
    this.findTeamResp();
    this.findTeams();
  }

  findTeamResp(){
    this.teamRespService.findTeamRespById(this.id).subscribe(b=>{
      return this.adminUser=b as BaseUser;
    },
    err=> {
      console.log(err);
    })
  }

  findTeams(){
    this.teams = [];
    return this.teamService.findTeams().subscribe( 
      actionArray =>{
        this.teams = actionArray.map(item => {
          return {
            id: item.id,
            ...item
          } as Team
        });
      },
      reject =>{
        this.errorMessage = "Oeps, er is iets mis gelopen met de spelers op te halen"
      }
    )
  }

  getFullName():string{
    return this.adminUser.firstName+" "+this.adminUser.lastName;
  }

}
