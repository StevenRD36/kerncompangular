import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditNewsFeedComponent } from './edit-news-feed.component';

describe('EditNewsFeedComponent', () => {
  let component: EditNewsFeedComponent;
  let fixture: ComponentFixture<EditNewsFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditNewsFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditNewsFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
