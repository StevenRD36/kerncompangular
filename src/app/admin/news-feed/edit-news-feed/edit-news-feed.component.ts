import { Component, OnInit, Input } from '@angular/core';
import { BoardMessagesService } from 'src/app/services/board-messages.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UploadServiceService } from 'src/app/services/upload-service.service';

@Component({
  selector: 'app-edit-news-feed',
  templateUrl: './edit-news-feed.component.html',
  styleUrls: ['./edit-news-feed.component.css']
})
export class EditNewsFeedComponent implements OnInit {

  @Input()
  selectedNewsFeed:BoardMessage
  imagePath;
  imgURL: any;
  message: string;
  file:File;
  isLoading:boolean

  registerNewsFeed = new FormGroup({
    title : new FormControl(''),
    message: new FormControl(''),
    image : new FormControl('')
  });


  constructor(
    private boardMessageService:BoardMessagesService,
    private uploadService:UploadServiceService
  ) { }

  ngOnInit() {
    if(this.selectedNewsFeed!=null){
    }
    this.isLoading = false
  }

  addImage(){
    console.log(this.registerNewsFeed.controls.title.value)
  }
 
  preview(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      console.log("not an image")
      this.message = "Only images are supported.";
      return;
    }
    this.file = files[0]
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
  }

  submitMessage(){
    this.isLoading = true
    var boardMessage = {
      title: this.registerNewsFeed.controls.title.value,
      content : this.registerNewsFeed.controls.message.value,
      creationDate : new Date(Date.now())
    } as BoardMessage

    if(this.file !== null){
      this.uploadService.uploadFile(this.file).subscribe(
        e => { 
          e.ref.getDownloadURL().then(
            e=>{
              console.log(e)
              boardMessage.image = e
              this.addMessage(boardMessage);
            },
            err => {
              console.log(err)
              this.isLoading = false;
            }

          )
        }
      )
    }
    else{
      this.addMessage(boardMessage)

    }

  }

  addMessage(boardMessage:BoardMessage){
    this.boardMessageService.addMessage(boardMessage).then(
      res=>{
        this.isLoading = false
        console.log(res)
      },
      err =>{
        this.isLoading = false
        console.log(err)
      }
    )
  }

}
