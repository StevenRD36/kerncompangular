import { Component, OnInit } from '@angular/core';
import { BoardMessagesService } from 'src/app/services/board-messages.service';
import { reject } from 'q';

@Component({
  selector: 'app-news-feed',
  templateUrl: './news-feed.component.html',
  styleUrls: ['./news-feed.component.css']
})
export class NewsFeedComponent implements OnInit {
  errorMessage : string;
  messages:BoardMessage[];
  displayedColumns:string[] = ["#","Datum","Titel","Bericht","Actie"];
  isSelected:boolean;

  constructor(private boardMessageService:BoardMessagesService) { }

  ngOnInit() {
    this.getBoardMessages()
    this.isSelected = true;
  }

  getBoardMessages(){
    this.messages = []
    this.boardMessageService.findMessages().subscribe(
      actionArray => {
        this.messages = actionArray.map(item =>{
          return item
        })
      },
      err => {
        this.errorMessage = "Er is iets misgelopen bij het binnenhalen van de berichten"
        reject(err)

      }
    )
  }


}
