import { Component, OnInit } from '@angular/core';
import {FormGroup,FormControl, Validators, ValidatorFn} from '@angular/forms';
import { Team } from '../domain/Team';
import { ActivatedRoute, Router } from '@angular/router';
import { TeamService } from '../services/team.service';
import { PlayerService } from '../services/player.service';
import { Player } from '../domain/Player';
import {NgbDatepickerConfig, NgbCalendar, NgbDate, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { Timestamp } from 'rxjs/internal/operators/timestamp';


@Component({
  selector: 'app-register-player',
  templateUrl: './register-player.component.html',
  styleUrls: ['./register-player.component.css']
})
export class RegisterPlayerComponent implements OnInit {
  errorMessage : string;
  successMessages: string;

  isLoading : Boolean
  selectedPlayer : Player
  displayedColumns:string[] = ["#","Voornaam","Naam","Geboortedatum","Actie"];
  players:Player[]

  registerPlayer = new FormGroup({
    firstName : new FormControl('',[Validators.required]),
    lastName : new FormControl('',[Validators.required]),
    birthdate : new FormControl('',[Validators.required])
  });

  id?;
  team:Team;

  datePickerModel:NgbDateStruct

  constructor(private route:ActivatedRoute,private teamService:TeamService,
    private playerService:PlayerService,private router:Router,private config:NgbDatepickerConfig) { 
    }

  ngOnInit() {
    this.config.minDate = {year:1960,month:1,day:1}
    this.config.maxDate = {year:2004, month:12,day:31}
    this.id = this.route.snapshot.paramMap.get('id');
    this.getTeam(this.id);
    this.isLoading = false;
    this.getPlayers(this.id);
    this.selectedPlayer = null
  }

  getPlayers(teamId:string){
    this.players = []
    this.playerService.findAllPlayersForTeam(teamId).subscribe(
          actionArray =>{
            this.players = actionArray.map(item => {
              return {
                id: item.payload.doc.id,
                ...item.payload.doc.data()
              } as Player
            });
          },
          reject =>{
            this.errorMessage = "Oeps er is iets mis gelopen met de spelers op te halen"
          }
        )
  }

  getTeam(id:string){
    this.teamService.findTeamById(this.id).then(
      snapshot => {
      this.team = snapshot; 
      this.team.id = id;
      console.log(this.team);}
    )
  }

  addPlayerToTeam(){
    if(this.registerPlayer.invalid){
      this.errorMessage = "Alle velden zijn verplicht in te vullen"
      return
    }
    this.toggleLoading()
    let firstName = this.registerPlayer.controls.firstName.value;
    let lastName = this.registerPlayer.controls.lastName.value;
    let birthdate = this.registerPlayer.controls.birthdate.value;
    console.log(birthdate);
    let birtdateDate = new Date();
    birtdateDate.setFullYear(birthdate.year,birthdate.month-1,birthdate.day)
   // birtdateDate.setMonth(birthdate.month - 1)
   // birtdateDate.setDate(birthdate.day) 
    let bString = birthdate.year+"-"+(birthdate.month-1)+"-"+birthdate.day
    console.log(birtdateDate)
    let p = this.playerService.createPlayer(firstName,lastName,birtdateDate,this.team.id,bString);
    this.playerRegister(p) 
  }

  resetFields(){
    this.registerPlayer.reset()
    this.selectedPlayer = null
  }

  private playerRegister(p:Player){
     return this.playerService.addPlayerForTeam(p).then( 
       res=>{
      this.toggleLoading()
      this.resetFields()
      this.getPlayers(this.id)
      this.successMessages='Speler '+p.firstName+ ' werd toegevoegd';
    },
    err =>{
      this.toggleLoading()
      this.errorMessage = 'Speler werd niet toegevoegd, probeer opnieuw';
    })
  }

  toggleLoading(){
    this.isLoading = !this.isLoading
  }

  changePlayer(p:Player){
    this.selectedPlayer = p
    let split = p.birthdateString.split("-")
    let year = 0
    let month = 0
    let day = 0
    if(split.length==3){
       year = parseInt(split[0])
       month= parseInt(split[1])+1
       day = parseInt(split[2])
    }

    let date:NgbDateStruct = {year:year,month:month,day:day}

    this.registerPlayer = new FormGroup({
      firstName : new FormControl(p.firstName,[Validators.required]),
      lastName : new FormControl(p.lastName,[Validators.required]),
      birthdate : new FormControl(date,[Validators.required])
    });
  }

  updatePlayer(){
    this.toggleLoading()
    if(this.registerPlayer.invalid || this.selectedPlayer==null){
      this.errorMessage = "Alle velden moeten verplicht ingevuld worden"
      return;
    }
    this.selectedPlayer.firstName = this.registerPlayer.controls.firstName.value;
    this.selectedPlayer.lastName = this.registerPlayer.controls.lastName.value;
    let birthdate = this.registerPlayer.controls.birthdate.value;
    let birtdateDate = new Date();
    birtdateDate.setFullYear(birthdate.year,birthdate.month-1,birthdate.day)

    let bString = birthdate.year+"-"+(birthdate.month-1)+"-"+birthdate.day
    this.selectedPlayer.birthdate = birtdateDate
    this.selectedPlayer.birthdateString = bString
    //update

    this.playerService.updatePlayer(this.selectedPlayer).then(
      res => {
        this.toggleLoading()
        this.successMessages = "Speler "+this.selectedPlayer.firstName+" werd aangepast"
        this.resetFields()
      },
      err => {
        this.errorMessage = "Er werd niets gewijzigd, probeer later opnieuw!"
      }
    )
  }


}
