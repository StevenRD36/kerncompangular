import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegisterComponent} from './register/register.component';
import {TeamComponent} from './team/team.component';
import { TeamsComponent } from './admin/teams/teams.component';
import { LoginComponent } from './login/login.component';
import { RegisterPlayerComponent } from './register-player/register-player.component';
import { AuthGuard } from './core/auth.guard';
import { AdminComponent} from './admin/admin.component';
import {HomeComponent} from './home/home.component';
import { TeamDetailComponent } from './admin/teams/team-detail/team-detail.component';
import { RetrievePasswordComponent } from './login/retrieve-password/retrieve-password.component';
import { InfoGlobalComponent } from './info-global/info-global.component';

const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'information', component: InfoGlobalComponent },
  { path: 'teams/:id', component:TeamComponent, canActivate:[AuthGuard] },
  { path: 'admin/:id/teams', component:TeamsComponent, canActivate:[AuthGuard]},
  { path: 'admin/:id/teams/:teamId',component:TeamDetailComponent, canActivate:[AuthGuard]},
  { path: 'login', component:LoginComponent},
  { path: 'teams/:id/register-player', component:RegisterPlayerComponent},
  { path: 'admin/:id', component:AdminComponent},
  { path: '',component:HomeComponent},
  { path: '404',redirectTo:''},
  { path: '**',redirectTo:''},
  { path: 'passwordReset', component:RetrievePasswordComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
