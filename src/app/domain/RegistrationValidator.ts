import { FormGroup } from '@angular/forms';

export class RegistrationValidator {
    static validate(registerTeam: FormGroup) {
        let password =registerTeam.get('teamResp.password').value
        let repeatPassword = registerTeam.get('teamResp.repeat').value;

        if( repeatPassword != null){

    
        if ( repeatPassword.length <= 0) {
            return null;
        }

        if (repeatPassword !== password) {
            return {
                doesMatchPassword: true
            };
        }
    }
        return null;

    }
}