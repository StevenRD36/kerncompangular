import { Team } from './Team';

export interface Player{
    id?;
    firstName:String;
    lastName:String;
    team:String;
    registerDate:Date;
    confirmDate?:Date;
    confirmed:boolean;
    birthdate:Date;
    confirmedAdminName:String;
    declined:boolean;
    birthdateString:string;
}