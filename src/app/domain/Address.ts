export class Address{

    street:string;
    number:string;
    bus:string;
    postalcode:number;
    city:string;

    constructor (  street:string,
        number:string,
        bus:string,
        postalcode:number,
        city:string,){
            this.street = street;
            this.number = number;
            this.bus = bus;
            this.postalcode = postalcode;
            this.city = city;
        }

    getFullAddress():string{
        return this.street+" "+this.number+" "+this.bus+"<br>"+
            this.postalcode+" "+this.city;
    }
}