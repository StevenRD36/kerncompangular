import { Address } from './Address';

export class BaseUser {
    id:string;
    firstName:string;
    lastName:string;
    email:string;
    phone:string;
    address:Address;
    teamId?:string;
    roles:string[];
    teamName:string;

    fullName= this.getFullName();

    getFullName():string{
        return this.firstName+" "+this.lastName;
    }

    getFullAddress():string{
        if(this.address == null){
            return "";
        }
        return this.address.getFullAddress();
    }
}