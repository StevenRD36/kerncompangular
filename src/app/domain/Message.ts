import { Team } from './Team';
import { BaseUser } from './BaseUser';

export class Message{
    sender:string;
    senderName:string;
    receiver:string;
    receiverName:string;
    subject : string;
    body : string;
    date: Date;
    seen : boolean;
    dateSeen : Date;
    
}

