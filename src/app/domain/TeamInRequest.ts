import { Address } from './Address';


export interface TeamInRequest{
    id?;
    name:string;
    resp:string;
    respEmail:string;
    respName:string;
    respFirstName:string;
    address:Address;
    registrationDate:Date;
}