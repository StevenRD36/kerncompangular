interface BoardMessage {
    id?;
    title:string;
    content:string;
    image?:string;
    creationDate:Date;
}