import { BaseUser } from './BaseUser';
import { Player } from './Player';

export class Team {

    id?;
    name:string;
    resp:string;
    respName:string;
    confirmed:boolean;
    confirmedDate:Date;
    confirmAdmin:string;
    registrationDate:Date;
    changeRequestDate: Date;
    changeRequestAdmin:string;
    changeRequest:boolean;
    payed : boolean;
    payedConfirmDate : Date;

    constructor(name:string,resp:string){
        this.name = name;
        this.resp = resp;
        this.confirmed = false;
        this.registrationDate = new Date(Date.now());
        this.confirmedDate = null;
        this.changeRequestDate = null;
        this.changeRequest = false;
        this.payed = false;
        this.payedConfirmDate = null;
    }


}