import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { auth } from 'firebase';
import { TeamService } from '../services/team.service';
import { BaseUser } from '../domain/BaseUser';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HeaderComponent } from '../header/header.component';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, OnDestroy {
  
  
  ngOnDestroy(): void {
    console.log("onDestroy");
  }
  isNavbarCollapsed:boolean = true;
  user:string;
  id:string;
  uid:string;
  roles:string[];
  isAdmin:boolean;
  isLoading:boolean;

  constructor(public auth:AuthService,
    private teamService:TeamService,
    private router:Router) { }

  ngOnInit() {
    this.isLoading = true;
    this.auth.user.subscribe(e=>{
      if(e){
        this.roles=e.roles;
        this.checkAdmin();
        this.user=e.email;
        console.log(this.roles)
        this.uid = e.id;
        this.findTeamForResp(e.id);
      }else{
        this.isLoading = false;
      }
    });
  }


  findTeamForResp(uid:string){
    this.teamService.findTeamByResp(uid).then(
      snapshot => {
        this.isLoading = false;
        this.id = snapshot.id}
    )
  }

  isLoggedIn():boolean{
    console.log(this.auth.isNotLoggedIn());
    return false;
  }

  checkAdmin(){
    if(this.roles!=null&&this.roles.length!=0){
      if(this.roles.includes("admin")){
        this.isAdmin = true;
      }
      else{
        this.isAdmin= false;
      }
    }
    else{
      this.isAdmin = false;

    }

  }

  logOut(){
    this.auth.user = null;
    this.user = null;
    this.id = null;
    this.auth.logOff().then(()=>{
      this.router.navigate(["/"]);
    })
  }
  
}
