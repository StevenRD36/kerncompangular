import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { LoginMessageService } from '../services/login-message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,OnDestroy {
  
  
  ngOnDestroy(): void {
    console.log("onDestroy login")
  }

  succes:string
  error:string
  isLoading : boolean;
  forgotPassword:boolean;


  login = new FormGroup({
    email : new FormControl(''),
    password : new FormControl('')
  })

  recoverPassword = new FormGroup({
    email : new FormControl("",[Validators.required,Validators.email])})
  
  constructor(private authService:AuthService,
    private router:Router,
      public loginMessages : LoginMessageService
    ) { }

  ngOnInit() {
    this.isLoading = false;
    this.forgotPassword = false;
  }

  forgotPasswordToggle(){
    this.error = null
    this.succes = null
    this.forgotPassword = !this.forgotPassword
  }

  tryLogin(){
    this.succes = null
    this.error = null
    this.isLoading = true;
    let email = this.login.controls.email.value;
    let password = this.login.controls.password.value;
    this.authService.doLogin(email,password).then(
      //do something
      res => {
        this.isLoading = false;
        this.succes ="Succesvolle login";
        this.router.navigate(['']);
      },
      err => {
        this.isLoading = false;
        console.log(err);
        this.error = "e-mail of paswoord worden niet herkend!"
      }
    )
  }

  sendEmail(){
    this.succes = null
    this.error = null
    if(this.recoverPassword.controls.email.valid){
      return this.authService.sendPasswordResetMail(this.recoverPassword.controls.email.value).then(
        res => {
          this.succes = "Een e-mail werd verzonden naar uw account"
          console.log(res)
      })
      .catch(
        error => {
          console.log(error)
          this.error="Het e-mailadres werd niet herkend bij ons!"
        }
      )
    }
    this.error = "E-mail adres is niet correct"
  }

}
