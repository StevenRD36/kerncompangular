import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { TeamService } from '../services/team.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  welcome = "Hallo, welkom"
  content = "Kerncompetitie Herzele is nog op zoek naar zaalvoetbalploegen. Meld jouw ploeg hieronder aan.";
  
  teamId:string;
  teamName:string;
  
  constructor(public auth:AuthService,
    private teamService:TeamService,
    private router:ActivatedRoute) { }
  
  ngOnInit() {
    console.log("on init home");
  }



  findTeamForResp(uid:string){
    this.teamService.findTeamByResp(uid).then(
      snapshot => {
        this.teamId = snapshot.id;
        return this.teamName = snapshot.name;
        },
        err =>{ return "";}
    )
  }


  checkAdmin(roles:string[]){
    console.log("Checking admin")

    if(roles!=null&&roles.length!=0){
      if(roles.includes("admin")){
        return true;
      }
      else{
        return false;
      }
    }
    else{
      return false;
    }

  }

}
