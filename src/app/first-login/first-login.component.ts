import { Component, OnInit } from '@angular/core';
import { LoginMessageService } from '../services/login-message.service';
import { AuthService } from '../services/auth.service';
import { Team } from '../domain/Team';
import { TeamService } from '../services/team.service';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-first-login',
  templateUrl: './first-login.component.html',
  styleUrls: ['./first-login.component.css']
})
export class FirstLoginComponent implements OnInit {

  messages:string[];
  userName:string;
  team:string;
  teamId:string;

  constructor(public activeModal:NgbActiveModal,private router:Router) { }

  ngOnInit() {
  }

  close(){
    this.router.navigate(['/teams',this.teamId]);
    this.activeModal.close();
  }

}
