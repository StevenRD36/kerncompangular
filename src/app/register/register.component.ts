import { Component, OnInit } from '@angular/core';
import {BaseUser} from '../domain/BaseUser';
import {FormGroup,FormControl, Validators, ValidatorFn} from '@angular/forms';
  import {TeamService} from '../services/team.service';
import { Team } from '../domain/Team';
import { TeamRespService } from '../services/team-resp.service';
import { LoginMessageService } from '../services/login-message.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FirstLoginComponent } from '../first-login/first-login.component';
import { AuthService } from '../services/auth.service';
import { RegistrationValidator } from '../domain/RegistrationValidator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  teamRespPerson : BaseUser;
  team : Team;

  registerTeam :FormGroup

  isLoading:Boolean;
  errorMessage : string;
  successMessage : string[];
  
  constructor( private auth:AuthService,
    private teamService:TeamService,
    private teamRespService:TeamRespService,
    private loginMessages:LoginMessageService,
    private router:Router,
    private modal:NgbModal) {
      this.errorMessage = null;
     }

  confirmPasswordValidator(): ValidatorFn{
    return (control: FormControl): {} | null => {
      const forbidden = control.value != this.registerTeam.get('teamResp.password').value
      return forbidden ? {'forbiddenName': {value: control.value}} : null;
    };
  }

  createFormGroup(){
    this.registerTeam = new FormGroup({
      name : new FormControl('',[Validators.required]),
    teamResp : new FormGroup({
      firstName : new FormControl('',[Validators.required]),
      lastName : new FormControl('',[Validators.required]),
      email : new FormControl('',[Validators.email,Validators.required]),
      password: new FormControl('',[Validators.required,Validators.minLength(8)]),
  //      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])$')]),
      repeat:new FormControl('',[Validators.required])})
    },{validators : RegistrationValidator.validate.bind(this)});
  }

  ngOnInit() {
    this.isLoading = false;
    this.successMessage = [];
    this.createFormGroup()
    console.log(this.registerTeam)

  }

  tryRegister(){
    this.toggleLoading()
    if(this.registerTeam.invalid){
      this.errorMessage = "Gelieve alle velden in te vullen"
    }
    else{
      var value = {
        email: this.registerTeam.get('teamResp.email').value,
        password : this.registerTeam.get('teamResp.password').value
      }
      return this.auth.doRegister(value).then(result =>
        {
          this.successMessage.push("Account werd aangemaakt, Gebruiker "+value.email)
          this.errorMessage = null
          this.tryCreateTeamResp(result.uid)
        },
        error => {
          this.toggleLoading()
          this.errorMessage = error.message
        });
    }


  }

  reset(){
    console.log(" in reset clicked");
    this.errorMessage = null;
    this.createFormGroup()
    console.log(this.registerTeam.valid)
  }
  tryCreateTeam(uid:string){
    console.log("in tryCreateTEam");
    this.team = new Team(this.registerTeam.controls.name.value,uid)
    this.team.registrationDate = new Date();
    this.team.respName = this.registerTeam.get('teamResp.firstName').value+" "+this.registerTeam.get('teamResp.lastName').value;
    return this.teamService.addTeam(this.team).then(res => {
      console.log(res);
      this.errorMessage = "";
      this.successMessage.push("Team werd aangemaakt");
      this.loginMessages.successMessages = this.successMessage;
      this.loginMessages.isFirstLoggin = true;
      this.teamRespPerson.teamId = this.team.id;
      this.teamRespPerson.teamName = this.team.name;
      this.teamRespService.updateResp(this.teamRespPerson).then(
        res => {
          this.toggleLoading();
          this.open();
        },
        err=>{
          console.log(err);
          this.errorMessage = err.message;
        }
      )

    }, err => {
      console.log(err);
      this.errorMessage = err.message;
    })
  }

  tryCreateTeamResp(uid:string){
    let firstName =this.registerTeam.get('teamResp.firstName').value;
    let lastName =this.registerTeam.get('teamResp.lastName').value;
    let email =  this.registerTeam.get('teamResp.email').value;
    let phone = "";
    this.teamRespPerson = new BaseUser();
    this.teamRespPerson.firstName = firstName;
    this.teamRespPerson.lastName = lastName;
    this.teamRespPerson.phone = phone;
    this.teamRespPerson.email=email;
    this.teamRespPerson.id = uid;
    this.teamRespPerson.roles = ['teamresp'];
    return this.teamRespService.addReps(this.teamRespPerson).then(
      res =>  {this.errorMessage = "";
      this.successMessage.push("Verantwoordelijke werd toegevoegd");
      this.tryCreateTeam(uid)
    },
      err => {
      this.toggleLoading()
      this.errorMessage = err.message;
      }    
      )
  }



  open(){
    const modalRef = this.modal.open(FirstLoginComponent);
    modalRef.componentInstance.messages = this.successMessage;
    modalRef.componentInstance.userName = this.teamRespPerson.firstName;
    modalRef.componentInstance.team = this.team.name;
    modalRef.componentInstance.teamId = this.team.id;
  }

  toggleLoading(){
    this.isLoading = !this.isLoading
  }

}
