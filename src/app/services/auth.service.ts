import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import {AngularFirestore, AngularFirestoreDocument} from 'angularfire2/firestore'
import {Observable,of} from 'rxjs';
import { BaseUser } from '../domain/BaseUser';
import { switchMap, map } from 'rxjs/operators';
import { TeamRespService } from './team-resp.service';
import { User } from 'firebase';
import { resolve, reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user:Observable<BaseUser>;
  isLoggedIn : Observable<boolean>;
  
  constructor(public fbAuth:AngularFireAuth,private fs:AngularFirestore,private teamRespService:TeamRespService) { 
    console.log("auth activated");
    this.getAuthState();

  }

  getCurrentUser(){
    return new Promise<any>((resolve,reject)=>{
      var user = this.fbAuth.auth.onAuthStateChanged(user => {
        if(user){
          console.log(user)
          resolve(user)
        }
        else{
          reject("no user")
        }
      })
    })
  }

  getAuthState(){  
     this.user = this.fbAuth.authState.pipe(
        switchMap(fbuser => {
          console.log("Check user")
          if(fbuser){
            this.isLoggedIn = of(true);
            return this.teamRespService.findTeamRespById(fbuser.uid);
          }
          else{
            this.isLoggedIn = of(false);
            return of(null);
          }
        }))
  }

  getUserId(){
    return this.user.subscribe(e=>{
      if(e){
        return e.id;
      }
      else{
        return "";
      }
    });
  }

  doRegister(value){
    console.log(value)
    return new Promise<User>((resolve,reject)=>this.fbAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
      .then(result => {
        console.log(result.user);
        this.getAuthState()
        resolve(result.user);
      },
      error=>{
        reject(error);
      }))
  }

  isNotLoggedIn(){
    this.user.subscribe(user => {
      console.log("in notLoggedIn")
      console.log(user);
      return user == null});
  }

  doLogin(email:string,password:string){
    return new Promise<User>((resolve,reject)=> this.fbAuth.auth.signInWithEmailAndPassword(email,password)
    .then(result=>{
      resolve(result.user);
      this.getAuthState()
      console.log(result);
    },
    error=>{
      reject(error);
    }))
  }


  logOff(){
    return this.fbAuth.auth.signOut();
  }

  sendPasswordResetMail(email:string){
    return this.fbAuth.auth.sendPasswordResetEmail(email)
  }

  
}
