import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Team } from '../domain/Team';
import { Message } from '../domain/Message';
import { resolve, reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  
  messageCollection : AngularFirestoreCollection<Message>

  constructor(private firestore:AngularFirestore) {
    this.messageCollection = firestore.collection<Message>('messages');

   }

  addMessage(message:Message){   
    let id = this.firestore.createId();
     return this.messageCollection.doc(id).set(Object.assign({},message)).then(
    res => {
      resolve(res);
    },
    err => {
      reject(err);
    })
}

  getMessagesForTeamResp(respId:any){
    return this.firestore.collection<Message>('messages',ref=>ref.where("receiver","==",respId))
   .snapshotChanges()
}



}
