import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { UploadServiceService } from './upload-service.service';
import { resolve, reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class BoardMessagesService {

  boardMessages : AngularFirestoreCollection<BoardMessage>;

  constructor(private firestore:AngularFirestore,private uploadService:UploadServiceService) {
    this.boardMessages = firestore.collection<BoardMessage>("boardMessages");
   }

   findMessages(){
    return this.boardMessages.valueChanges();
  }

  addMessage(boardMessage:BoardMessage){
    return this.boardMessages.add(boardMessage);
  }
}
