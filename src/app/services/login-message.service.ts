import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginMessageService {

  successMessages:string[];
  isFirstLoggin:boolean;
  notAuthMessages:string[];

  constructor() {
    console.log("login-message on login");
    this.isFirstLoggin = false;
    this.successMessages = [];
    this.notAuthMessages = [];
   }

   addSuccessMessage(message:string){
     console.log(message);
     this.successMessages.push(message);
   }

   addNotAuthMessages(message:string){
     console.log(message);
     this.notAuthMessages.push(message);
   }

   resetMessages(){
     this.successMessages = [];
     this.notAuthMessages = [];
     
   }
   
}
