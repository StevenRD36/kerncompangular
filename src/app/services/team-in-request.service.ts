import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { GlobalVariables } from '../domain/GlobalVariables';
import { TeamInRequest } from '../domain/TeamInRequest';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { resolve, reject } from 'q';


@Injectable({
  providedIn: 'root'
})
export class TeamInRequestService{

  teamInRequests : AngularFirestoreCollection<TeamInRequest>;

  constructor(private variables:GlobalVariables,private firestore:AngularFirestore) {
    this.teamInRequests = firestore.collection<TeamInRequest>(variables.TEAMS_IN_REQUEST);

   }

   addTeamRequest(team:TeamInRequest){
    team.id = this.firestore.createId();
    return this.teamInRequests.doc(team.id).set(Object.assign({},team)).then(
      res => {
        console.log(res);
        resolve(res);
      },
      err => {
        reject(err);
      }
    )
  }

  getAllTeamsInRequest(){
    return this.teamInRequests.valueChanges();
  }

   
}
