import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";
import { Team } from '../domain/Team';
import { resolve, reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  teamCollection : AngularFirestoreCollection<Team>;

  constructor(private firestore:AngularFirestore) {
    this.teamCollection = firestore.collection<Team>('teams');
   }

  addTeam(team:Team){
    team.id = this.firestore.createId();
    return this.teamCollection.doc(team.id).set(Object.assign({},team)).then(
      res => {
        console.log(res);
        resolve(res);
      },
      err => {
        reject(err);
      }
    )
  }

  updateTeam(team:Team){
    return this.teamCollection.doc(team.id).update(team).then(
      res => {
        console.log(res),
        resolve(res)},
      err => {
        console.log(err),
        reject(err)}
    )
  }

  findTeams(){
    return this.teamCollection.valueChanges()
  }

  findTeamById(id:string){
    return new Promise<any>((resolve,reject)=>{
      this.teamCollection.doc(id).valueChanges().subscribe(
        snapshot => {resolve(snapshot);},
        reject => {reject(reject);}
      )
    })
  }

  findTeamByResp(uid:string){
     return new Promise<Team>((resolve,reject)=>{
       this.firestore.collection<Team>('teams',ref=>ref.where("resp","==",uid))
    .snapshotChanges().subscribe(
      snapshot=>{
        if(snapshot.length==1){
          console.log(snapshot[0].payload.doc.id);
          resolve(snapshot[0].payload.doc.data());
        }
        else{
          reject("Geen team bij deze verantwoordelijke");
        }
    },
    error => {
      console.log(reject);
      reject(reject);
    })
  })
}
}
