import { TestBed } from '@angular/core/testing';

import { TeamRespService } from './team-resp.service';

describe('TeamRespService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeamRespService = TestBed.get(TeamRespService);
    expect(service).toBeTruthy();
  });
});
