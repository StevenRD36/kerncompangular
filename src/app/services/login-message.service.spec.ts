import { TestBed } from '@angular/core/testing';

import { LoginMessageService } from './login-message.service';

describe('LoginMessageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoginMessageService = TestBed.get(LoginMessageService);
    expect(service).toBeTruthy();
  });
});
