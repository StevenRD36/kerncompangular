import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";
import { BaseUser } from '../domain/BaseUser';

@Injectable({
  providedIn: 'root'
})
export class TeamRespService {

  teamRespCollection : AngularFirestoreCollection<BaseUser>;

  constructor(private firestore:AngularFirestore) {
    this.teamRespCollection = firestore.collection<BaseUser>('teamresp');
   }


  findTeamRespById(uid:string){
    return this.teamRespCollection.doc<BaseUser>(uid).valueChanges();
  }

  addReps(teamResPerson:BaseUser){
    return this.teamRespCollection.doc(teamResPerson.id).set(Object.assign({},teamResPerson)).then(
      res=>{ 
        console.log("success");
        console.log(res);
      },
      err=>{ 
        console.log("something went wrong");
        console.log(err);
      }
    );
  }

  updateResp(teamResPerson:BaseUser){
    return this.teamRespCollection.doc(teamResPerson.id).update(Object.assign({},teamResPerson))
  }
  
}
