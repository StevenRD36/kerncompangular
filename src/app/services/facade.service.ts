import { Injectable } from '@angular/core';
import { TeamService } from './team.service';
import { PlayerService } from './player.service';
import { TeamRespService } from './team-resp.service';
import { Player } from '../domain/Player';
import { PlayersComponent } from '../team/players/players.component';

@Injectable({
  providedIn: 'root'
})
export class FacadeService {


  private players:Player[];

  constructor(private teamService:TeamService,
    private playerService:PlayerService,private teamRespService:TeamRespService) {
      this.players = [];
     }

  getPlayers(){
    if(this.players==null||this.players.length==0){
      
    }
  }

}
