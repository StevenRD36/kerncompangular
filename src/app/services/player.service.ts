import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";
import { Team } from '../domain/Team';
import { resolve, reject } from 'q';
import { Player } from '../domain/Player';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  playerCollection : AngularFirestoreCollection<Player>
  constructor(private firestore:AngularFirestore) {
    this.playerCollection = firestore.collection<Player>('players');
   }

  findAllPlayers(){
    return this.playerCollection.valueChanges();
  }

  findAllPlayersForTeam(teamId:string){
    return this.firestore.collection<Player>('players',ref=>ref.where('team','==',teamId))
      .snapshotChanges();
  }

  playerAlreadyExists(firstName:String,lastName:String,birthdate:string){
     return this.firestore.collection<Player>('players',ref=>ref.where('firstName','==',firstName)
    .where('lastName','==',lastName)
    .where('birthdateString','==',birthdate))
    .get()
  }

  addPlayerForTeam(player:Player){
    
    console.log(player)
    return this.playerCollection.add(Object.assign(player))
  }

  updatePlayer(player:Player){
    return this.playerCollection.doc(player.id).update(player).then(
      res=>{
        resolve(res);
      },
      err =>{
        reject(err);
      }
    )
  }

  createPlayer(firstName:String,lastName:String,birthdate:Date,team:string,birthdateString:string){
      let player:Player = {
      firstName : firstName,
      lastName : lastName,
      birthdate : birthdate,
      team : team,
      birthdateString: birthdateString,
      registerDate : new Date(Date.now()),
      confirmed : false,
      confirmedAdminName : "",
      declined : false
      }
      return player
  }
}
