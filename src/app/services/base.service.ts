import { Injectable } from '@angular/core';
import { GlobalVariables } from '../domain/GlobalVariables';
import {AngularFirestore, AngularFirestoreCollection} from "angularfire2/firestore";


@Injectable({
  providedIn: 'root'
})
export class BaseService {

  constructor(variables:GlobalVariables,protected firestore:AngularFirestore) { }
}
