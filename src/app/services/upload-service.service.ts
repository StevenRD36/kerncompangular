import { Injectable } from '@angular/core';
import {AngularFireStorage, AngularFireUploadTask} from 'angularfire2/storage'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadServiceService {

  task : AngularFireUploadTask
  percentage:Observable<number>
  snapshot:Observable<any>
  downLoadUrl: Observable<string>

  constructor(private storage:AngularFireStorage) { }


  uploadFile(file:File){
    
    if(file.type.split("/")[0]!=="image"){
      console.log("unsupported file")
      return;
    }
    
    var path = 'images/'+file.name

    this.task = this.storage.upload(path,file)
    this.percentage = this.task.percentageChanges();
    return this.task.snapshotChanges()

  }
}
