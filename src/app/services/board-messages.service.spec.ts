import { TestBed } from '@angular/core/testing';

import { BoardMessagesService } from './board-messages.service';

describe('BoardMessagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BoardMessagesService = TestBed.get(BoardMessagesService);
    expect(service).toBeTruthy();
  });
});
