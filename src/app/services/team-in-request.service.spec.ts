import { TestBed } from '@angular/core/testing';

import { TeamInRequestService } from './team-in-request.service';

describe('TeamInRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TeamInRequestService = TestBed.get(TeamInRequestService);
    expect(service).toBeTruthy();
  });
});
