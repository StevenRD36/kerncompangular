import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoGlobalComponent } from './info-global.component';

describe('InfoGlobalComponent', () => {
  let component: InfoGlobalComponent;
  let fixture: ComponentFixture<InfoGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
