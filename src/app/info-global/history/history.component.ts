import { Component, OnInit } from '@angular/core';
import { HistoryService } from '../history.service';
import { HistoryComp } from 'src/app/domain/History';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  history : HistoryComp[]
  currentItem : HistoryComp;
  counter = 0;
  previousButton = "";
  nextButton = "";

  constructor(private historyService:HistoryService) { }

  ngOnInit() {
    this.history = this.historyService.history;
    this.getHistoryItem();
  }

  getNextButtonText(){
    if(this.counter < this.history.length-1){
      this.nextButton = "arrow-circle-right"
    }
    else if(this.counter == this.history.length-1){
      this.nextButton = "home"
    }
  }

  getPreviousButtonText(){
      if(this.counter > 0){
        this.previousButton = "arrow-circle-left"
      }
      else{
        this.previousButton = ""
      }
  }

  nextStory(){
    if(this.counter < this.history.length){
      this.counter ++;
      this.getHistoryItem(); 
    }
  }

  previousStory(){
    if(this.counter>0){
      this.counter --;
      this.getHistoryItem();
    }
  }

  getHistoryItem(){
    if(this.counter == this.history.length){
      this.counter = 0;
    }
    if(this.counter < this.history.length){
      this.currentItem = this.history[this.counter];
    }
    this.getNextButtonText();
    this.getPreviousButtonText();
  }


}
