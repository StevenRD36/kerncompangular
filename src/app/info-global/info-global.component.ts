import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialService } from 'ng6-social-button';

@Component({
  selector: 'app-info-global',
  templateUrl: './info-global.component.html',
  styleUrls: ['./info-global.component.css']
})
export class InfoGlobalComponent implements OnInit {


  shareObj = {
    href:"https://kernherzele.firebaseapp.com/",
    hashtag:""
  }

  constructor(private router:Router,private socialAuthService: SocialService) { }

  ngOnInit() {
    console.log("test")
  }
  

}
