import { Injectable } from '@angular/core';
import { HistoryComp } from '../domain/History';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  history : HistoryComp[];

  constructor() { 
    this.generateHistory();

  }

  generateHistory(){
    this.history = [];
    this.history.push({
      imgUrl : "https://firebasestorage.googleapis.com/v0/b/kernherzele.appspot.com/o/images%2FFoto-01.svg?alt=media&token=a331e29c-bb14-48e2-bdb5-c54272fee301",
      text : [
        "Eind jaren '90 groeide -vooral op aandringen van wat lokale cafés- de nood aan een indoor voetbal-competite in Herzele. Als grensgebied tussen twee regio's met populaire voetbalsporten (Erpe-Mere, Aalst, ... : zaalvoetbal & Zottegem, Zwalm, ... : minivoetbal) werd geopteerd voor het zaalvoetbal (mét doelman…)." ,
        "Ondanks de niet-vanzelfsprekende aanvangsuren op zondagnamiddag, kende het concept al vrij snel succes. In de eerste seizoenen gingen de competitiewedstrijden door in het Gemeentelijk Sportcentrum. Vanaf 2003 werd het splinternieuwe sportcomplex De Steenoven de vaste locatie."
      ]
    },{
      imgUrl : "https://firebasestorage.googleapis.com/v0/b/kernherzele.appspot.com/o/images%2FFoto-02.svg?alt=media&token=518c1661-d7b3-41d0-82db-bf676da93894",
      text : [
        "Doorheen de jaren zijn verschillende ploegen de revue gepasseerd. Op het hoogtepunt telde de competitie 15 teams. Sommige ploegen maakten nadien de stap naar de Provinciale reeksen, of combineerden zelf beiden.",      
        "De enige ploeg die er van bij de start bij was en nu nog steeds actief is, is ZVC Lappetem. Zij konden in 2012 hun 25-jarig bestaan vieren met een kampioenstitel."  
      ]
    },{
      imgUrl:"https://firebasestorage.googleapis.com/v0/b/kernherzele.appspot.com/o/images%2FFoto-03.svg?alt=media&token=7727c4dc-bea7-44a9-a6e8-4ff92cc5da2d",
      text: [
        "De Roeligans tekenen ook al sinds 2002 present. In hun eerste jaren maakten ze furore met TV-gastoptredens en deelnames aan buitenlandse toernooien en een paar titels.",
        "De ploeg met de meeste kampioenstitels is trouwens ZVC De Promillekes, een hecht team met een vrij constante bezetting. De laatste jaren konden ook andere teams als The Sinner en Atlético B hun seizoen afsluiten als competitiewinnaar."
      ]
    }, {
      imgUrl:"https://firebasestorage.googleapis.com/v0/b/kernherzele.appspot.com/o/images%2FAffiche-HKZ-A2.svg?alt=media&token=5af1b056-c79f-43ae-87ea-2767f5148964",
      text: [
        "Inmiddels kennen de meeste ploegen extra sportieve activiteiten om hun kas te spijzen. De jaarlijkse Lappetem-quiz is een begrip in de regio, de Roeligans zorgen voor sfeer met hun OktoberFests.",
        "De Promillekes voorzien groot-Herzele van gesmaakte ontbijtpakketten op Paaszondag. De Slekkelekkers verkopen dan weer heerlijke gebakjes. Andere ploegen organiseren hun jaarlijks eetfestijn.",
        "De competitie was tot 2019 verbonden aan de KBVB, met een eigen stamnummer. Voor het seizoen 2019-2020 probeert men het van een andere boeg te gooien, en is de competitie in handen van een eigen organisatie.",
        "Dit in samenwerking met het gemeentebestuur en de immer sympathieke uitbater van het Steenoven-sportcafé, Guy De Moor."
      ]
    }
    )
  }
}
