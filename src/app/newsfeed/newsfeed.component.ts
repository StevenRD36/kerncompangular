import { Component, OnInit } from '@angular/core';
import { BoardMessagesService } from '../services/board-messages.service';

@Component({
  selector: 'app-newsfeed',
  templateUrl: './newsfeed.component.html',
  styleUrls: ['./newsfeed.component.css']
})
export class NewsfeedComponent implements OnInit {

  messages : BoardMessage[]
  rowClass : string
  cardClass : string
  selectedMessage : BoardMessage

  constructor(private boardMessageService:BoardMessagesService) { }

  ngOnInit() {
    this.getMessages();
  }

  getMessages(){
    this.messages = [];
    return this.boardMessageService.findMessages().subscribe( 
      actionArray =>{
        this.messages = actionArray.map(item => {
          console.log(item)
          return item
        });
        if(this.messages.length>0){
          console.log(this.messages.length)
          this.selectedMessage = this.messages[0]
          console.log(this.selectedMessage)
        }
      },
      reject =>{
        console.log("getmessages went wrong")
      }
    )
  }

  selectMessage(m:BoardMessage){
    this.selectedMessage = m
  }
}
