import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Message } from 'src/app/domain/Message';

@Component({
  selector: 'app-team-messages',
  templateUrl: './team-messages.component.html',
  styleUrls: ['./team-messages.component.css']
})
export class TeamMessagesComponent implements OnInit {

  id:string;
  @Input() messages:Message[];
  errorMessage:string;
  displayedColumns:string[] = ["Verzonden","Verzender","Onderwerp","Bericht"];


  constructor(private route:ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
  }


  
}


