import { Component, OnInit, Input } from '@angular/core';
import { PlayerService } from 'src/app/services/player.service';
import { Player } from 'src/app/domain/Player';
import { Address } from 'src/app/domain/Address';
import { Team } from 'src/app/domain/Team';
import { ActivatedRoute } from '@angular/router';
import { Timestamp } from 'rxjs';
import { ItemsList } from '@ng-select/ng-select/ng-select/items-list';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit {
  errorMessage:string;
  @Input() players:Player[];
  id:string;
  displayedColumns:string[] = ["#","Voornaam","Naam","Geregistreerd op","Bevestigd op"];


  constructor(private playerService:PlayerService,private route:ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
  }


}
