import { Component, OnInit, Input } from '@angular/core';
import { Team } from 'src/app/domain/Team';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  @Input()
  team:Team;

  constructor() { }

  ngOnInit() {
  }

}
