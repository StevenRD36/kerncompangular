import { Component, OnInit } from '@angular/core';
import { Team } from '../domain/Team';
import {ActivatedRoute} from '@angular/router';
import { TeamService } from '../services/team.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Player } from '../domain/Player';
import { PlayerService } from '../services/player.service';
import { reject } from 'q';
import { BaseUser } from '../domain/BaseUser';
import { TeamRespService } from '../services/team-resp.service';
import { Address } from '../domain/Address';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { StringHelper } from '../domain/string.helper';
import { Message } from '../domain/Message';
import { MessageService } from '../services/message.service';


@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  team:Team;
  id?;
  displayedColumns:string[] = ["number","firstName","lastName","registration","confirmation"];
  successMessage:string;
  errorMessage:string;
  teamResp:BaseUser;
  players:Player[];
  messages:Message[];


  registerPlayer = new FormGroup({
    firstName : new FormControl(''),
    lastName : new FormControl('')
  });
  
  constructor(private route:ActivatedRoute,
    private teamService:TeamService,
    private playerService:PlayerService,
    private teamRespService:TeamRespService,
    private messageService:MessageService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getTeam(this.id);
    this.getPlayers(this.id);
  }

  getFullNameTeamResp():string{
    return StringHelper.getFullName(this.teamResp.firstName,this.teamResp.lastName);
  }

  private reset(){
    this.registerPlayer.controls.firstName = new FormControl('');
    this.registerPlayer.controls.lastName = new FormControl('');
  }

  getTeamResp(id:string){
    console.log(id);
    this.teamRespService.findTeamRespById(this.team.resp).subscribe(
      data => {
        console.log(data);
        return this.teamResp = data as BaseUser;
      },
      err =>{
        console.log("TeamResp not found");
      }
    )
  }

  getTeam(id:string){
    this.teamService.findTeamById(this.id).then(
      snapshot => {
      this.team = snapshot; 
      this.team.id = id;
      this.getTeamResp(this.team.resp);
      this.findMessagesForTeamResp(this.team.resp)}
    )
  }

  getPlayers(id:string){
    console.log("in get players")
    this.players = [];
    return this.playerService.findAllPlayersForTeam(id).subscribe( 
      actionArray =>{
        this.players = actionArray.map(item => {
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as Player
        });
      },
      reject =>{
        this.errorMessage = "Oeps er is iets mis gelopen met de spelers op te halen"
      }
    )
  }

  findMessagesForTeamResp(id:string){
    this.messages = []
    this.messageService.getMessagesForTeamResp(id).subscribe(
      actionArray =>{
        this.messages = actionArray.map(item => {
          console.log(item)
          return {
            ...item.payload.doc.data()
          } as Message
        });
      },
      reject =>{
        this.errorMessage = "Oeps er is iets mis gelopen met de spelers op te halen"
      }
    )
  }

}
