import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../services/auth.service';
import { LoginMessageService } from '../services/login-message.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor(private authService : AuthService, 
    private router:Router,
    public afAuth: AngularFireAuth,
    private loginMessagesService: LoginMessageService){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    console.log("canActivate start")
    return new Promise((resolve,reject)=>{
      this.authService.user.subscribe( u =>{
        if(u){
          console.log(u)
          return resolve(true)
        }
        else{
          console.log("not user")
          this.router.navigate["/"]
          return resolve(false)
        }
      },
      err => {
        console.log(err)
        this.router.navigate["/"]
        return resolve(false)
      }
      )
    })
  }
}
