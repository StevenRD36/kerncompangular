import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';
import {MatTableModule,MatToolbarModule,MatMenuModule,MatIconModule} from '@angular/material';
import { NgbModule, NgbActiveModal,NgbAlertModule ,NgbModalModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {environment} from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './register/register.component';
import { TeamComponent } from './team/team.component';
import { TeamsComponent } from './admin/teams/teams.component';
import { LoginComponent } from './login/login.component';
import { NavigationComponent } from './navigation/navigation.component';
import { PlayerDetailComponent } from './player-detail/player-detail.component';
import { RegisterPlayerComponent } from './register-player/register-player.component';
import { CompetitionComponent } from './competition/competition.component';
import { PlayersComponent } from './team/players/players.component';
import {NgSelectModule,NgOption} from '@ng-select/ng-select';
import { FirstLoginComponent } from './first-login/first-login.component';
import { AdminComponent } from './admin/admin.component';
import { MessagesComponent } from './admin/messages/messages.component';
import { HomeComponent } from './home/home.component';
import { NewsfeedComponent } from './newsfeed/newsfeed.component';
import { TeamDetailComponent } from './admin/teams/team-detail/team-detail.component';
import { TeamMessagesComponent } from './team/team-messages/team-messages.component';
import { HeaderComponent } from './header/header.component';
import { StandComponent } from './stand/stand.component';
import { NewsFeedComponent } from './admin/news-feed/news-feed.component';
import { EditNewsFeedComponent } from './admin/news-feed/edit-news-feed/edit-news-feed.component';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { ArticleComponent } from './newsfeed/article/article.component';
import { RetrievePasswordComponent } from './login/retrieve-password/retrieve-password.component';
import { InfoComponent } from './team/info/info.component';
import { InfoGlobalComponent } from './info-global/info-global.component';
import { FooterComponent } from './footer/footer.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HistoryComponent } from './info-global/history/history.component';
import { Ng6SocialButtonModule,SocialServiceConfig, SocialService } from 'ng6-social-button';

//661538747680942
export function getAuthServiceConfigs() {
  let config = new SocialServiceConfig()
      .addFacebook("661538747680942")
  return config;
}


@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    TeamComponent,
    TeamsComponent,
    LoginComponent,
    NavigationComponent,
    PlayerDetailComponent,
    RegisterPlayerComponent,
    CompetitionComponent,
    PlayersComponent,
    FirstLoginComponent,
    AdminComponent,
    MessagesComponent,
    HomeComponent,
    NewsfeedComponent,
    TeamDetailComponent,
    TeamMessagesComponent,
    HeaderComponent,
    InfoComponent,
    StandComponent,
    NewsFeedComponent,
    EditNewsFeedComponent,
    ArticleComponent,
    RetrievePasswordComponent,
    InfoGlobalComponent,
    FooterComponent,
    HistoryComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    BrowserAnimationsModule,
    MatTableModule,
    NgbModule.forRoot(),
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    NgbAlertModule,
    NgSelectModule,
    FormsModule,
    NgbModalModule,
    AngularFireStorageModule,
    AngularFontAwesomeModule,
    Ng6SocialButtonModule
    ],
  providers: [NgbActiveModal,{provide:SocialServiceConfig,useFactory:getAuthServiceConfigs}],
  bootstrap: [AppComponent],
  entryComponents: [FirstLoginComponent]

})
export class AppModule {
  constructor(){
  }
 }
